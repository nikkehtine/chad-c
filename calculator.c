// #include <math.h>
#include <stdio.h>

int main() {
    char operator;
    double num1, num2, result;

    printf("Enter a number: ");
    scanf("%lf", &num1);

    printf("What would you like to do with it? (+ - * /): ");
    scanf(" %c", &operator);

    printf("Enter a second number: ");
    scanf("%lf", &num2);

    switch (operator) {
    case '+':
        result = num1 + num2;
        break;
    case '-':
        result = num1 - num2;
        break;
    case '*':
        result = num1 * num2;
        break;
    case '/':
        if ((int)num2 == 0) {
            printf("\nError: division by zero");
            return 1;
        }
        result = num1 / num2;
        break;
    default:
        printf("\nError: %c is not a valid operator!", operator);
        return 1;
    }

    printf("\nThe result is %.2lf", result);

    return 0;
}
