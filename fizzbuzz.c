#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
    int num;

    switch (argc) {
    case 1:
        printf("Print FizzBuzz this many times: ");
        scanf("%d", &num);
        break;
    case 2:
        num = atoi(argv[1]);
        break;
    default:
        printf("Error: too many arguments\n");
        printf("Expected 2, got %d", argc);
        return 1;
    }

    bool printed = false;

    void printToScreen(char word[]) {
        printf("%s", word);
        printed = true;
    }

    for (int i = 1; i <= num; i++) {
        printed = false;

        if (i % 3 == 0) {
            printToScreen("Fizz");
        }
        if (i % 5 == 0) {
            printToScreen("Buzz");
        }

        if (!printed)
            printf("%d", i);

        printf("\n");
    }

    return 0;
}
