#include <stdio.h>
#include <math.h>

int main() {
  double radius;
  const double PI = 3.141592;
  double circumference;
  double area;

  printf("\nEnter radius of a circle: ");
  scanf("%lf", &radius);

  circumference = 2 * PI * radius;

  printf("\nThe circumference is %lf", circumference);

  area = PI * pow(radius, 2);

  printf("\nThe area is %lf", area);

  return 0;
}

