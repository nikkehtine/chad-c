#include <ctype.h>
#include <stdio.h>

int main() {
    char unit;
    float temp;

    printf("\nChoose the unit of temperature (C, F): ");
    scanf("%c", &unit);
    unit = toupper(unit);

    switch (unit) {
    case 'C':
        printf("\nEnter the temperature in Celsius: ");
        scanf("%f", &temp);
        temp = (temp * 9 / 5) + 32;
        printf("\nThe temp in Fahrenheit is %.1f", temp);
        break;
    case 'F':
        printf("\nEnter the temperature in Fahrenheit: ");
        scanf("%f", &temp);
        temp = (temp - 32) * 5 / 9;
        printf("\nThe temp in Celsius is %.1f", temp);
        break;
    default:
        printf("\nError: no such unit %c", unit);
    }

    return 0;
}
